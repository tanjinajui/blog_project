<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Input;
use Symfony\Component\Console\Input\Input;
use App\Divisions;
use App\Districts;
use App\Upozilas;
class AddressController extends Controller
{
    public function division {

    }
     public function district(Request $request){
        $divisions_id = $request->Input('divisions_id');
      $district = Districts::where('divisions_id', '=', $divisions_id)->get();
      return response()->json($district);
    }
}
