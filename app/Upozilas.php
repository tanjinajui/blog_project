<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Upozilas extends Model
{
   protected $fillable=['upozila_name','divisions_id','districts_id'];
}
