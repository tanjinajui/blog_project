@extends('layouts.includes.master')
@section('title', 'New_District')
@section('content')

<div class="page-heading">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <div class="page-title">
           <h2>District Add</h2>
          </div>
        </div>
        <!--col-xs-12-->
      </div>
      <!--row-->
    </div>
    <!--container-->
</div>    
       
<!-- BEGIN Main Container -->           
<div class="main-container col1-layout wow bounceInUp animated animated" style="visibility: visible;">     
    <div class="main">                     
      <div class="account-login container">
        <div class="heading text-center">
          <h3><a href="/districts">View All Districts</a></h3>
          <!-- registration heading field -->   
          <!-- <h1>Category Add</h1> -->
        </div>
      <!--form-part starts -->
      <form action="/districts" method="POST">
    <!-- form-part Starts -->
        <div class="col-md-3"></div>
        <fieldset class="col2-set ">
            <div class="col-1 new-users">
             <!-- include message page -->
            @include('messages.message')              
               <!-- Division_name field -->
              <div class="form-group">                 
                {{csrf_field()}}
                <label for="division_name">Division Name</label>
                <select name="divisions_id" class="form-control" id="">
                  <option value="">Please select a division</option>
                  @foreach($divisions as $division)
                  <option value="{{$division->id}}">{{$division->division_name}}</option>
                  @endforeach
                </select>
              </div> 
               <!-- District_name field -->
              <div class="form-group">                 
                <label for="district_name">District Name</label>
                <input type="text" name="district_name" class="form-control" id="district_name" placeholder="District Name">
              </div>                                       
              <input type="submit" name="create_district" class="btn btn-primary btn-user btn-block mt-5" value="District Add">
          </div>         
        </fieldset> <!--col2-set-->
      </form>
    
  </div><!--main-container-->
          
</div> <!--col1-layout-->


@endsection('content')


