@extends('layouts.includes.master')
@section('title', 'New_Upozila')
@section('content')

<div class="page-heading">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <div class="page-title">
           <h2>Upozila Add</h2>
          </div>
        </div>
        <!--col-xs-12-->
      </div>
      <!--row-->
    </div>
    <!--container-->
</div>    
       
<!-- BEGIN Main Container -->           
<div class="main-container col1-layout wow bounceInUp animated animated" style="visibility: visible;">     
    <div class="main">                     
      <div class="account-login container">
        <div class="heading text-center">
          <h3><a href="/upozila">View All Upozila</a></h3>
          <!-- registration heading field -->   
          <!-- <h1>Category Add</h1> -->
        </div>
      <!--form-part starts -->
      <form action="/upozila" method="POST">
    <!-- form-part Starts -->
        <div class="col-md-3"></div>
        <fieldset class="col2-set ">
            <div class="col-1 new-users">
             <!-- include message page -->
            @include('messages.message')  
              <!-- Division_name field -->
              <div class="form-group">                 
                {{csrf_field()}}
                <label for="division_name">Division Name</label>
                <select name="divisions_id" class="form-control" id="division">
                  <option value="">Please select a division</option>
                  @foreach($divisions as $division)
                  <option value="{{$division->id}}">{{$division->division_name}}</option>
                  @endforeach
                </select>
              </div>           
               <!-- Districts_name field -->
              <div class="form-group">  
               <select name="districts_id" class="form-control" id="district">  
                <option value="">Please select a District</option>                           
                </select>
              </div> 
              <!-- Upozila_name field -->
              <div class="form-group">                                
                <input type="text" name="upozila_name" class="form-control" id="upozila_name" placeholder="Upozila Name">
              </div>                                       
              <input type="submit" name="create_upozila" class="btn btn-primary btn-user btn-block mt-5" value="Upozila Add">
          </div>         
        </fieldset> <!--col2-set-->
      </form>
    
  </div><!--main-container-->
          
</div> <!--col1-layout-->



<section class="row">
<div class="col-lg-2"></div>

<div class="col-lg-6 ">
    @include('messages.message')
    <section class="card ">
        <header class="card-header">
             Add Upazila
        </header>
        <div class="card-body ">
          <form action="{{url('upazilas')}}" method="post">
                @csrf
                <div class="form-group form-row">
                    <label for="departmetName" class="col-md-3 col-form-label"> Division Name </label>
                    <select class="form-control col-md-9" id="division"  placeholder="Division Name " name="divisions_id"> 
                        <option value="">Select Your Divition</option>
                        @foreach($divisions as $data)
                            <option value="{{$data->id}}">{{$data->division_name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group form-row">
                    <label for="departmetName" class="col-md-3 col-form-label"> Division Name </label>
                    <select class="form-control col-md-9" id="district"  placeholder="Division Name " name="districts_id"> 
                        <option value="">Select Your Divition</option>
                        {{-- @foreach($district as $data)
                            <option value="{{$data->id}}">{{$data->district_name}}</option>
                        @endforeach --}}
                   </select>
                </div>

                <div class="form-group form-row">
                    <label for="departmetName" class="col-md-3 col-form-label"> Upazila Name </label>
                    <input type="text" class="form-control col-md-9" id="departmetName"  placeholder="Upazila Name " name="upazila_name">
                </div>

                <button type="submit" class="btn btn-primary">Submit</button>
            </form> 

        </div>
    </section>

</div>

</section>

@endsection('content')
<!-- apnei to java scritpt oi add  koren nai kaj korbe kibane  
javascript add koren bai apni bolen ki korte hobe sir er video dekhe korlam apni bolen 
javascript ta add koren kaj hoe jabe  -->
<!-- linux install kora ase  na apner js file ta koje ber kore add koren ki add korbo-->


